# How to use it

### Instal zsh

```bash
	dnf install zsh
or
	apt install zsh
```

### Install antigen
```bash
curl -L git.io/antigen > ~/appz/antigen.zsh
```

https://github.com/zsh-users/antigen

### mod zshrc
```bash
cp zshrc.david ~/.zshrc
```

### Mod user default shell

```bash
sudo usermod -s /bin/zsh $USERNAME
```


### Dependencies
* gcloud sdk
* aws-cli
* crudini


----
Enjoy the magic
----
![](images/ss_prompt.png)
